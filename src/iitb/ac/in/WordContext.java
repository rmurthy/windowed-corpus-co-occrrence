package iitb.ac.in;

import java.util.Map;
import java.util.TreeMap;

public class WordContext 
{
	String word;
	TreeMap<Integer, Double> vector;
	
	public WordContext()
	{
		vector = new TreeMap<>();
	}
	
	public void normalize()
	{
		double sum = 0.0;
		for (Map.Entry<Integer, Double> entry1 : vector.entrySet())
		{
			sum += entry1.getValue() * entry1.getValue();
		}
		if(sum != 0.0)
		{
			sum = 1.0 / Math.sqrt(sum);
			for (Map.Entry<Integer, Double> entry1 : vector.entrySet())
			{
				vector.put(entry1.getKey(), sum);
			}
		}
	}
}
