package iitb.ac.in;

import iitb.ac.in.Utilities.MapUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Main 
{
	public static String DELIMITERS = "[ \t]+";
			 
	public static String SPACE = " ";
	public static String SPACEORTAB = "[ \t]";
	
	public static String DIGITS = "\\d+";
	
	public static String DIGITIZER = "<NUMBER>";
	public static String TAG = "<TAG>";
	public static String AMPERSAND = "&";
	public static String UNKNOWN = "<UNK>";
	public static String TAB = "\t";
	public static String NEWLINE = "\n";
	
	public static void main(String[] args) 
	{
		if(args.length != 4)
		{
			System.out.println("Usage: inputFile outputFile lineNumber columnVocabulary");
			System.exit(-1);
		}
		
		Map<String, Integer> vocabulary = new TreeMap<>();
		Map<String, Integer> columnVocabulary = new TreeMap<>();
		
		createVocabularyFile(args[0], vocabulary);
		
		vocabulary.clear();
		
		createVocabulary("vocabularyComplete", vocabulary);
		System.out.println("Reduced Vocabulary Size " + vocabulary.size());
		
		//Create Column Vocabulary
		createVocabularyContent("vocabularyModified",vocabulary,columnVocabulary, "vocabularyComplete");
		
		System.out.println("Column contains " + columnVocabulary.size() + " words");
		
		int lineNumber = Integer.valueOf(args[2]);
		Boolean debug = false;
		
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(args[0]));
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(args[1]));
			String line = null;
			
			int count = 0, index = 1;
			
		    long start = System.nanoTime();
			
			while((line = br.readLine()) != null)
			{
				if(count%100000 == 0)
				{
					long diff = System.nanoTime() - start;
					
					System.out.println("Time taken = " + (diff / 1000000000) + " seconds");
					start = System.nanoTime();
				}
				
				if(lineNumber != 0)
				{
					if(index == lineNumber)
					{
						lineNumber = 0;
					}
					else
					{
						index++;
						continue;
					}
				}
				
				line = line.trim();
				
				if(line.isEmpty())
				{
					continue;
				}
			
				if(line.split(SPACEORTAB).length <= 2)
				{
					count++;
					continue;
				}
				
				index++;
				
				if(debug)
					System.out.println(line);
				
				count++;
				
				line = line.replaceAll(DELIMITERS, SPACE);
				line = line.toLowerCase();
				line = line.replaceAll(DIGITS, DIGITIZER);
				
				String[] words = line.split(SPACEORTAB);
				
				int indexWord= 0;
				
				for(int i = 0; i < words.length; i++)
				{
					if(words[i].startsWith(AMPERSAND))
					{
						words[i] = TAG;
					}
					
					if(vocabulary.containsKey(words[i]))
					{
						bw.write(words[i] + SPACE);
					}
					else
					{
						continue;
						//bw.write(vocabulary.get("<UNK>") + " ");
					}
					
					for(int j = 0; j < words.length ; j++)
					{
						if(j < 0 || j >= words.length || i == j)
							continue;
						
						if(columnVocabulary.containsKey(words[j]))
						{
							indexWord = columnVocabulary.get(words[j]);
							
							bw.write(indexWord + SPACE);
						}
					}
					bw.newLine();
				}
			}
			br.close();
			bw.close();
			
			vocabulary.clear();
			
			ConstructMatrix.createVocabulary("vocabularyColumnWiki", ConstructMatrix.columnVocabulary);
			ConstructMatrix.readFile(args[1], ConstructMatrix.columnVocabulary);
			
			//readFile(args[1], columnVocabulary);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void createVocabularyFile(String filename, Map<String, Integer> vocab)
	{
		long count = 0;
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(filename));
			
			String line = null;
			String[] words ;
			
			while((line = br.readLine()) != null)
			{
				line = line.trim();
				if(line.isEmpty())
					continue;
				
				line = line.replaceAll(DELIMITERS, SPACE);
				line = line.toLowerCase();
				line = line.replaceAll(DIGITS, DIGITIZER);
				
				words = line.split(SPACEORTAB);
				
				for(int i = 0; i < words.length; i++)
				{
					if(words[i].startsWith(AMPERSAND))
					{
						words[i] = TAG;
					}
					
					if(vocab.containsKey(words[i]))
					{
						vocab.put(words[i], vocab.get(words[i]) + 1);
					}
					else
					{
						vocab.put(words[i], 1);
						count++;
//						if(count %100000 == 0)
//						{
//							System.out.println("\rRead " + count + " number of words");
//						}
					}
				}
			}

			br.close();
			
			vocab = MapUtil.sortByValue(vocab);
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("vocabularyComplete"));
			
			Iterator it = null;

	        it = vocab.entrySet().iterator();

	        while(it.hasNext())
	        {
	        	Map.Entry pairs = (Map.Entry) it.next();

                bw.write((String) pairs.getKey() + TAB + pairs.getValue() );
                bw.newLine();
            }
            bw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public static void createVocabulary(String filename, Map<String, Integer> vocab)
	{
		long count = 0;
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(filename));
			
			String line = null;
			int index = 0;
			String[] words ;
			
			while((line = br.readLine()) != null)
			{
				count++;
				line = line.trim();
				if(line.isEmpty())
					continue;
				
				words = line.split(SPACEORTAB);
				
				if(words.length != 2)
				{
					System.out.println(line + "problem at line " + count);
				}
				
				if(Integer.valueOf(words[1]) < 10 || Integer.valueOf(words[1]) > 100000)
				{
					if(!vocab.containsKey(UNKNOWN))
					{
						vocab.put(UNKNOWN, index);
						index++;
					}
				}
				else
				{
					if(!vocab.containsKey(words[0]))
					{
						vocab.put(words[0], index);
						index++;
					}
					else
					{
						System.out.println("Duplicate entry " + words[0]);
					}
				}
			}
			br.close();
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("vocabularyModified"));
			
			Iterator it = null;

	        it = vocab.entrySet().iterator();

	        while(it.hasNext())
	        {
	        	Map.Entry pairs = (Map.Entry) it.next();

                bw.write((String) pairs.getKey() + TAB + pairs.getValue() + NEWLINE); 
            }
            bw.close();

		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public static void createVocabulary(String filename, Map<String, Integer> vocab,
			Map<String, Integer> columnVocab)
	{
		long count = 0;
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(filename));
			
			String line = null;
			int index = 0;
			String[] words ;
			
			while((line = br.readLine()) != null)
			{
				count++;
				line = line.trim();
				if(line.isEmpty())
					continue;
				
				words = line.split(SPACE);
				
				if(words.length != 2)
				{
					System.out.println(line + "problem at line " + count);
				}
//				else
//					vocab.put(words[0], Integer.valueOf(words[1]));
				
				if(Integer.valueOf(words[1]) < 0 || Integer.valueOf(words[1]) > 170000)
				{
					if(!columnVocab.containsKey(UNKNOWN))
					{
						columnVocab.put(UNKNOWN, index);
						index++;
					}
				}
				else
				{
					if(!columnVocab.containsKey(words[0]))
					{
						columnVocab.put(words[0], index);
						index++;
					}
					else
					{
						System.out.println("Duplicate entry " + words[0]);
					}
				}
			}
			br.close();
			columnVocab = MapUtil.sortByValue(columnVocab);
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("res/vocabularyColumnWiki"));
			
			Iterator it = null;

	        it = columnVocab.entrySet().iterator();

	        while(it.hasNext())
	        {
	        	Map.Entry pairs = (Map.Entry) it.next();

                bw.write((String) pairs.getKey() + TAB + pairs.getValue() + NEWLINE); 
            }
            bw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public static void createVocabularyContent(String filename, Map<String, Integer> vocab,
			Map<String, Integer> columnVocab, String columnFilename)
	{
		long count = 0;
		try
		{
			vocab.clear();
			columnVocab.clear();
			
			BufferedReader br = new BufferedReader(new FileReader(filename));
			
			String line = null;
			int index = 0;
			String[] words ;
			
			while((line = br.readLine()) != null)
			{
				count++;
				line = line.trim();
				if(line.isEmpty())
					continue;
				
				words = line.split(SPACEORTAB);
				
				if(words.length != 2)
				{
					System.out.println(line + "problem at line " + count);
				}
				else
					vocab.put(words[0], Integer.valueOf(words[1]));
			}
			br.close();
			
			br = new BufferedReader(new FileReader(columnFilename));
			
			index = 0;
			
			while((line = br.readLine()) != null)
			{
				count++;
				line = line.trim();
				if(line.isEmpty())
					continue;
				
				words = line.split(SPACEORTAB);
				
				if(words.length != 2)
				{
					System.out.println(line + "problem at line " + count);
				}
				
				if(Integer.valueOf(words[1]) < 100 || Integer.valueOf(words[1]) > 17000)
				{
					if(!columnVocab.containsKey(UNKNOWN))
					{
						columnVocab.put(UNKNOWN, index);
						index++;
					}
				}
				else
				{
					if(!columnVocab.containsKey(words[0]))
					{
						columnVocab.put(words[0], index);
						index++;
					}
					else
					{
						System.out.println("Duplicate entry " + words[0]);
					}
				}
			}
			br.close();
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("vocabularyColumnWiki"));
			
			Iterator it = null;

	        it = columnVocab.entrySet().iterator();

	        while(it.hasNext())
	        {
	        	Map.Entry pairs = (Map.Entry) it.next();

                bw.write((String) pairs.getKey() + TAB + pairs.getValue() + NEWLINE); 
            }
            bw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	static void readFile(String inFile, Map<String, Integer> vocabulary)
	{
		try
		{
			Set<Integer> columns = new HashSet<>();
			BufferedReader br = new BufferedReader(new FileReader(inFile));
				
			Map<String,WordContext> words = new TreeMap<>();
			
			String line = null;
			int lineNumber = 0;
			
			//Strin
			
			String[] word;
			//int[] ints ;
			String currentWord ;
			WordContext temp;
			
			while((line = br.readLine()) != null)
			{
				line = line.trim();
				
				word = line.split(SPACEORTAB);
				lineNumber++;
				if(word.length == 1)
					continue;
				
				if(lineNumber%1000000 == 0)
				{
					System.out.println("\rReading line Number " + lineNumber);
				}
				
				currentWord = word[0];
				
				if(words.containsKey(currentWord))
				{
					for(int i = 1; i < word.length; i++)
					{
						Integer tempWord = Integer.valueOf(word[i]);
						
						if(words.get(currentWord).vector.containsKey(tempWord))
						{
							words.get(currentWord).vector.put(tempWord, 
									words.get(currentWord).vector.get(tempWord) + 1.0);
						}
						else
						{
							words.get(currentWord).vector.put(tempWord, 1.0);
						}
					}
				}
				else
				{
					temp = new WordContext();
					
					for(int i = 1; i < word.length; i++)
					{
						Integer tempWord = Integer.valueOf(word[i]);
						
						if(temp.vector.containsKey(tempWord))
						{
							temp.vector.put(tempWord, 
									temp.vector.get(tempWord) + 1.0);
						}
						else
						{
							temp.vector.put(tempWord, 1.0);
						}
					}
					
					temp.word = currentWord;
					
					words.put(currentWord,temp);
				}
			}
			br.close();
			
			//standardize(words, columns);
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("cooccurrenceFile"));
			
			for (Map.Entry<String, WordContext> entry : words.entrySet())
			{
				WordContext t = entry.getValue();
				
				bw.write(t.word + SPACE );
					
				int index = 0;
				
				for (Map.Entry<Integer, Double> entry1 : t.vector.entrySet())
				{
					int index1 = entry1.getKey();
					double value = entry1.getValue();
					
//					while(index <  index1)
//					{
//						bw.write("0 ");
//						index++;
//					}
//					
//					bw.write(value + SPACE);
					
					bw.write(index1 + ":" + value + SPACE);
				}
				bw.newLine();
				bw.flush();
			}
			bw.close();
			
			columns.clear();
			words.clear();

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
