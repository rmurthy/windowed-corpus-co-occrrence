package iitb.ac.in;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class ConstructMatrix 
{
	public static String DELIMITERS = "[ \t]+";
	 
	public static String SPACE = " ";
	public static String SPACEORTAB = "[ \t]";
	
	public static String DIGITS = "\\d+";
	
	public static String DIGITIZER = "<NUMBER>";
	public static String TAG = "<TAG>";
	public static String AMPERSAND = "&";
	public static String UNKNOWN = "<UNK>";
	public static String TAB = "\t";
	public static String NEWLINE = "\n";
	
	static Map<String, Integer> columnVocabulary = new TreeMap<>();
	
	static void readFile(String inFile, Map<String, Integer> vocabulary)
	{
		try
		{
			Set<Integer> columns = new HashSet<>();
			BufferedReader br = new BufferedReader(new FileReader(inFile));
				
			Map<String,WordContext> words = new TreeMap<>();
			
			String line = null;
			int lineNumber = 0;
			
			String[] word;
			
			String currentWord ;
			WordContext temp;
			
			while((line = br.readLine()) != null)
			{
				line = line.trim();
				
				word = line.split(SPACEORTAB);
				lineNumber++;
				if(word.length == 1)
					continue;
				
				if(lineNumber%1000000 == 0)
				{
					System.out.println("\rReading line Number " + lineNumber);
				}
				
				currentWord = word[0];
				
				if(words.containsKey(currentWord))
				{
					for(int i = 1; i < word.length; i++)
					{
						Integer tempWord = Integer.valueOf(word[i]);
						
						columns.add(tempWord);
						
						if(words.get(currentWord).vector.containsKey(tempWord))
						{
							words.get(currentWord).vector.put(tempWord, 
									words.get(currentWord).vector.get(tempWord) + 1.0);
						}
						else
						{
							words.get(currentWord).vector.put(tempWord, 1.0);
						}
					}
				}
				else
				{
					temp = new WordContext();
					
					for(int i = 1; i < word.length; i++)
					{
						Integer tempWord = Integer.valueOf(word[i]);
						
						columns.add(tempWord);
						
						if(temp.vector.containsKey(tempWord))
						{
							temp.vector.put(tempWord, 
									temp.vector.get(tempWord) + 1.0);
						}
						else
						{
							temp.vector.put(tempWord, 1.0);
						}
					}
					
					temp.word = currentWord;
					
					words.put(currentWord,temp);
				}
			}
			br.close();
			
			standardize(words, columns);
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("cooccurrenceFile"));
			
			for (Map.Entry<String, WordContext> entry : words.entrySet())
			{
				WordContext t = entry.getValue();
				
				bw.write(t.word + SPACE );
					
				int index = 0;
				
				for (Map.Entry<Integer, Double> entry1 : t.vector.entrySet())
				{
					int index1 = entry1.getKey();
					double value = entry1.getValue();
					
					bw.write(index1 + ":" + value + SPACE);
				}
				bw.newLine();
				bw.flush();
			}
			bw.close();
			
			columns.clear();
			words.clear();

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void createVocabulary(String filename, Map<String, Integer> vocab)
	{
		long count = 0;
		try
		{
			vocab.clear();
			
			BufferedReader br = new BufferedReader(new FileReader(filename));
			
			String line = null;
			int index = 0;
			String[] words ;
			
			while((line = br.readLine()) != null)
			{
				count++;
				line = line.trim();
				if(line.isEmpty())
					continue;
				
				words = line.split(SPACEORTAB);
				
				if(words.length != 2)
				{
					System.out.println(line + "problem at line " + count);
				}
				else
					vocab.put(words[0], Integer.valueOf(words[1]));
			}
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	static void standardize(Map<String,WordContext> words, Set<Integer> columns)
	{
		Iterator<Integer> it = columns.iterator();
		int i = 0;
		while(it.hasNext()) 
		{
			int columnIndex = (int) it.next();
			double columnCount = 0;
			
			for(String key : words.keySet())
			{
				if(words.get(key).vector.containsKey(columnIndex))
				{
					columnCount += words.get(key).vector.get(columnIndex);
				}
			}
			if(columnCount != 0.0)
				for(String key : words.keySet())
				{
					if(words.get(key).vector.containsKey(columnIndex))
					{
						words.get(key).vector.put(columnIndex, words.get(key).vector.get(columnIndex) 
								/ columnCount);
					}
				}
		}
	}
	
	static void standardize(Map<Integer,WordContext> words, Set<Integer> columns, Map<Integer, Double> idf)
	{
		Iterator<Integer> it = columns.iterator();
		int i = 0;
		while(it.hasNext()) 
		{
			int columnIndex = (int) it.next();
			double columnCount = 0;
			
			int noOfContextWord = 0;
			Map<Integer,Double> count = new TreeMap<>();
			
			for(Integer key : words.keySet())
			{
				if(words.get(key).vector.containsKey(columnIndex))
				{
					columnCount += words.get(key).vector.get(columnIndex);
					noOfContextWord++;
				}
			}
			
			if(columnCount != 0.0)
				for(Integer key : words.keySet())
				{
					if(words.get(key).vector.containsKey(columnIndex))
					{
						words.get(key).vector.put(columnIndex, words.get(key).vector.get(columnIndex) 
								/ columnCount);
					}
				}
			
			for(Integer key : words.keySet())
			{
				if(words.get(key).vector.containsKey(columnIndex))
				{
					words.get(key).vector.put(columnIndex, words.get(key).vector.get(columnIndex) 
							* Math.log((words.size() * 1.0) / noOfContextWord));
				}
			}
		}
	}
}
