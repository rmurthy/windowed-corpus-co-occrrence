# README #

* This project constructs co-occurrence matrix using windowed approach, ready to be used directly or after dimensionality reduction for Word Similarity Tasks

## Preliminaries ##
This program expects the corpus to contain single sentence per line and the text to be tokenized.

### Main.java ###
This class contains the complete execution pipeline to the system. It reads in a corpus, constructs vocabulary, constructs co-occurrence statistics for every instance of the word and writes it to output file.

## ConstructMatrix.java ##
This class reads in the above output file and writes a co-occurrence matrix for every word in sparse notation. It can additionally perform normalization along columns or fill each entry in the corpus with values from statistical measures like PMI etc.

Eg: 
word1 1:123 4:20 200:2
word2 0:12 4:14 2000:23 2456:34

This sparse matrix can be used directly or fed into Matlab for dimensionality reduction and later used for Word Similarity Tasks.

### Running Using Jar file ###
windowedCooccurrenceConstruct.jar can be used to run the entire pipeline. The arguments to be provided are 

```
#!shell
inputFile outputFile lineNumber

```

* inputFile is the link to the tokenized corpus
* outputFile is the file to which the the co-occurrence words per instance of every word is written
* lineNumber is to resume the execution from a particular line number in the corpus
